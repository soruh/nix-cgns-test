{
    description = "very old VTK";

    inputs.nixpkgs.url = "github:nixos/nixpkgs";

    outputs = { self, nixpkgs }:
        let pkgs = nixpkgs.legacyPackages.x86_64-linux;
        in {
            packages.x86_64-linux.default =
                with import nixpkgs { system = "x86_64-linux"; };
                pkgs.stdenv.mkDerivation rec {
                    pname = "vtk";
                    version = "5.10.1";

                    src = pkgs.fetchurl {
                        url = "https://github.com/Kitware/VTK/archive/refs/tags/v${version}.tar.gz";
                        sha256 = "nRxPCk/ZAJmsTtkuiRAKSRagFbTW63jC+u+6X57wUTI=";
                    };

                    nativeBuildInputs = [ pkgs.cmake ];
                    buildInputs = [ pkgs.libpng pkgs.libtiff pkgs.libGL pkgs.libGLU pkgs.xorg.libX11 pkgs.xorg.xorgproto pkgs.xorg.libXt ];

		    # pkgs.mesa pkgs.glew

		    patchPhase = ''
		    	patch CMake/vtkCompilerExtras.cmake <<< '28c28
<   string (REGEX MATCH "[345]\\.[0-9]\\.[0-9]" 
---
>   string (REGEX MATCH "[0-9]+\\.[0-9]\\.[0-9]" '
		    '';

                    configurePhase = ''
		        cmake -Wno-dev -DCMAKE_C_FLAGS="-DGLX_GLXEXT_LEGACY" -DCMAKE_CXX_FLAGS="-DGLX_GLXEXT_LEGACY -std=c++03" -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$out" .
                    '';

                    buildPhase = ''
                        make -j"$NIX_BUILD_CORES"
                    '';

                    installPhase = ''
                        make install
                    '';
                };
        };
}
